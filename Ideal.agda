{-# OPTIONS --without-K --safe #-}

-- Two-sided ideals

open import Algebra using (Ring)

module Ideal {c} {ℓ} (A : Ring c ℓ) where

open import Agda.Primitive using (lsuc; _⊔_)

open Ring A renaming (Carrier to C)
open import Subgroup (Ring.+-group A) hiding (_∈_)

record Ideal ι : Set (c ⊔ ℓ ⊔ lsuc ι) where
  field
    subgroup : Subgroup ι

  open Subgroup subgroup public
    renaming (contains-ε to contains-0; mul to add; inv to neg)

  field
    lmul : (a : C) {b : C} → contains b → contains (a * b)
    rmul : (a : C) {b : C} → contains b → contains (b * a)

infix 2 _∈_

_∈_ : ∀ {ℓ} → C → Ideal ℓ → Set ℓ
a ∈ 𝕒 = Ideal.contains 𝕒 a
