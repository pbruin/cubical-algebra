{-# OPTIONS --cubical --safe #-}

-- Quotient of an Abelian group by a subgroup

open import Algebra using (AbelianGroup; Group)
open import Subgroup using (Subgroup)

module AbelianGroupQuotient {c} {ℓ} (A : AbelianGroup c ℓ)
  {ι} (H : Subgroup (AbelianGroup.group A) ι) where

open import Cubical.Core.Primitives using (_≡_; ℓ-max)

import GroupQuotient
import NormalSubgroup

AbelianGroupQuotient : AbelianGroup (ℓ-max c ι) (ℓ-max c ι)
AbelianGroupQuotient = record { isAbelianGroup = ⊕-isAbelianGroup } where

  open AbelianGroup A using (comm; group)

  N : NormalSubgroup.NormalSubgroup group ι
  N = record { subgroup = H ; normal = λ a b → Subgroup.contains-≈ H (comm a b) }

  open Group (GroupQuotient.GroupQuotient N) renaming (Carrier to A/H)
  open import Algebra using (IsAbelianGroup)
  open import Algebra.Definitions (_≡_ {A = A/H}) using (Commutative)

  ∙-comm : Commutative _∙_
  ∙-comm = Quotient-elim²-prop (set-Quotient _ _)
                               λ a b → []-congₗ (comm a b)
    where
      open import LeftRightQuotient H
      open import Quotient

  ⊕-isAbelianGroup : IsAbelianGroup _≡_ _∙_ ε _⁻¹
  ⊕-isAbelianGroup = record { isGroup = isGroup ; comm = ∙-comm }
