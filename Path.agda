{-# OPTIONS --cubical --safe #-}

module Path where

open import Cubical.Core.Primitives using (_≡_)
open import Cubical.Foundations.Prelude

open import Relation.Binary using (IsEquivalence)

congruence₂ : ∀ {ℓ ℓ′ ℓ″} {A : I → Type ℓ} {B : I → Type ℓ′}
              {C : I → Type ℓ″} (f : {i : I} → A i → B i → C i)
              {a : A i0} {a′ : A i1} {b : B i0} {b′ : B i1}
              (p : PathP A a a′) (q : PathP B b b′)
             → PathP C (f a b) (f a′ b′)
congruence₂ f p q = λ i → f (p i) (q i)

≡-isEquivalence : ∀ {ℓ} {A : Type ℓ} → IsEquivalence (_≡_ {A = A})
≡-isEquivalence = record
  {
    refl = refl ;
    sym = sym ;
    trans = _∙_
  }
