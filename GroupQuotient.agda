{-# OPTIONS --cubical --safe #-}

-- Quotient of a group by a normal subgroup

open import Algebra using (Group)
import NormalSubgroup

module GroupQuotient {c} {ℓ} {G : Group c ℓ}
  {ι} (N : NormalSubgroup.NormalSubgroup G ι) where

open import Cubical.Core.Primitives using (_≡_; ℓ-max)
open import Cubical.Data.Sigma using (_,_)
open import Cubical.HITs.SetQuotients
open import Cubical.Foundations.Prelude using (cong)

open Group G renaming (Carrier to C)
open NormalSubgroup G
open NormalSubgroup.NormalSubgroup N

open import LeftRightQuotient subgroup renaming (LeftQuotient to N\G)
open import Path using (congruence₂; ≡-isEquivalence)
open import Quotient

open import Algebra using (IsGroup)
open import Algebra.Definitions (_≡_ {A = N\G})

import Relation.Binary.Reasoning.Setoid
private module ≈-Reasoning = Relation.Binary.Reasoning.Setoid setoid
open ≈-Reasoning

GroupQuotient : Group (ℓ-max c ι) (ℓ-max c ι)
GroupQuotient = record { Carrier = N\G ; isGroup = ⊙-isGroup } where

  lemma : (g h : C) → h ⁻¹ ∙ g ⁻¹ ≈ (g ∙ h) ⁻¹
  lemma g h = inverseʳ-unique (g ∙ h) (h ⁻¹ ∙ g ⁻¹) eq
    where
      open import Algebra.Properties.Group G
      eq : g ∙ h ∙ (h ⁻¹ ∙ g ⁻¹) ≈ ε
      eq = begin
        g ∙ h ∙ (h ⁻¹ ∙ g ⁻¹)   ≈⟨ assoc g h (h ⁻¹ ∙ g ⁻¹) ⟩
        g ∙ (h ∙ (h ⁻¹ ∙ g ⁻¹)) ≈⟨ ∙-cong refl (sym (assoc h (h ⁻¹) (g ⁻¹))) ⟩
        g ∙ (h ∙ h ⁻¹ ∙ g ⁻¹)   ≈⟨ ∙-cong refl (∙-cong (inverseʳ h) refl) ⟩
        g ∙ (ε ∙ g ⁻¹)          ≈⟨ ∙-cong refl (identityˡ (g ⁻¹)) ⟩
        g ∙ g ⁻¹                ≈⟨ inverseʳ g ⟩
        ε                       ∎

  infixl 6 _⊙_

  lemma-⊙-1 : (a b : C) → a ∙ b ⁻¹ ∈ N → (c : C)
              → [ a ∙ c ] ≡ [ b ∙ c ]
  lemma-⊙-1 a b ab⁻¹∈N c = eq/ (a ∙ c) (b ∙ c) ac[bc]⁻¹∈N where
    eq : a ∙ b ⁻¹ ≈ a ∙ c ∙ (b ∙ c) ⁻¹
    eq = begin
      a ∙ b ⁻¹                ≈⟨ ∙-cong refl (sym (identityˡ (b ⁻¹))) ⟩
      a ∙ (ε ∙ b ⁻¹)          ≈⟨ ∙-cong refl (∙-cong (sym (inverseʳ c)) refl) ⟩
      a ∙ (c ∙ c ⁻¹ ∙ b ⁻¹)   ≈⟨ ∙-cong refl (assoc c (c ⁻¹) (b ⁻¹)) ⟩
      a ∙ (c ∙ (c ⁻¹ ∙ b ⁻¹)) ≈⟨ sym (assoc a c (c ⁻¹ ∙ b ⁻¹)) ⟩
      a ∙ c ∙ (c ⁻¹ ∙ b ⁻¹)   ≈⟨ ∙-cong refl (lemma b c) ⟩
      a ∙ c ∙ (b ∙ c) ⁻¹      ∎
    ac[bc]⁻¹∈N : a ∙ c ∙ (b ∙ c) ⁻¹ ∈ N
    ac[bc]⁻¹∈N = contains-≈ eq ab⁻¹∈N

  lemma-⊙-2 : (m a b : C) → a ∙ b ⁻¹ ∈ N → [ m ∙ a ] ≡ [ m ∙ b ]
  lemma-⊙-2 m a b ab⁻¹∈N = eq/ (m ∙ a) (m ∙ b) ma[mb]⁻¹∈N where
    eq : m ∙ (a ∙ b ⁻¹) ∙ m ⁻¹ ≈ m ∙ a ∙ (m ∙ b) ⁻¹
    eq = begin
      m ∙ (a ∙ b ⁻¹) ∙ m ⁻¹   ≈⟨ ∙-cong (sym (assoc m a (b ⁻¹))) refl ⟩
      m ∙ a ∙ b ⁻¹ ∙ m ⁻¹     ≈⟨ assoc (m ∙ a) (b ⁻¹) (m ⁻¹) ⟩
      m ∙ a ∙ (b ⁻¹ ∙ m ⁻¹)   ≈⟨ ∙-cong refl (lemma m b) ⟩
      m ∙ a ∙ (m ∙ b) ⁻¹      ∎
    ma[mb]⁻¹∈N : m ∙ a ∙ (m ∙ b) ⁻¹ ∈ N
    ma[mb]⁻¹∈N = contains-≈ eq (conj m ab⁻¹∈N)

  action : C → N\G → N\G
  action m = Quotient-rec set-Quotient (λ a → [ m ∙ a ]) (lemma-⊙-2 m)

  _⊙_ : N\G → N\G → N\G
  _⊙_ = Quotient-rec² set-Quotient (λ a b → [ a ∙ b ]) lemma-⊙-1 lemma-⊙-2

  lemma-! : (a b : C) → a ∙ b ⁻¹ ∈ N → [ a ⁻¹ ] ≡ [ b ⁻¹ ]
  lemma-! a b ab⁻¹∈N = eq/ (a ⁻¹) (b ⁻¹) a⁻¹[b⁻¹]⁻¹∈N where
    claim : (b ⁻¹) ⁻¹ ∙ a ⁻¹ ∈ N
    claim = contains-≈ (sym (lemma a (b ⁻¹))) (inv ab⁻¹∈N)
    a⁻¹[b⁻¹]⁻¹∈N : a ⁻¹ ∙ (b ⁻¹) ⁻¹ ∈ N
    a⁻¹[b⁻¹]⁻¹∈N = normal ((b ⁻¹) ⁻¹) (a ⁻¹) claim

  !_ : N\G → N\G
  !_ = Quotient-rec set-Quotient (λ a → [ a ⁻¹ ]) lemma-!

  ⊙-assoc : Associative _⊙_
  ⊙-assoc = Quotient-elim³-prop (set-Quotient _ _)
                                λ a b c → []-congₗ (assoc a b c)

  ⊙-idₗ : LeftIdentity [ ε ] _⊙_
  ⊙-idₗ = Quotient-elim-prop (set-Quotient _ _)
                             λ a → []-congₗ (identityˡ a)

  ⊙-idᵣ : RightIdentity [ ε ] _⊙_
  ⊙-idᵣ = Quotient-elim-prop (set-Quotient _ _)
                             λ a → []-congₗ (identityʳ a)

  ⊙-invₗ : LeftInverse [ ε ] !_ _⊙_
  ⊙-invₗ = Quotient-elim-prop (set-Quotient _ _)
                              λ a → []-congₗ (inverseˡ a)

  ⊙-invᵣ : RightInverse [ ε ] !_ _⊙_
  ⊙-invᵣ = Quotient-elim-prop (set-Quotient _ _)
                              λ a → []-congₗ (inverseʳ a)

  ⊙-isGroup : IsGroup _≡_ _⊙_ [ ε ] (!_)
  ⊙-isGroup = record
    {
      isMonoid = record
        {
          isSemigroup = record
            {
              isMagma = record
                {
                  isEquivalence = ≡-isEquivalence ;
                  ∙-cong = congruence₂ _⊙_
                } ;
              assoc = ⊙-assoc
            } ;
          identity = ⊙-idₗ , ⊙-idᵣ
        } ;
      inverse = ⊙-invₗ , ⊙-invᵣ ;
      ⁻¹-cong = cong (!_)
    }
