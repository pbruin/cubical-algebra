{-# OPTIONS --without-K --safe #-}

-- Miscellaneous identities in an Abelian group

open import Algebra
open import Level

module AbelianGroupIdentities {c ℓ : Level} (A : AbelianGroup c ℓ) where

open import Data.Product
import Relation.Binary.Reasoning.Setoid

open AbelianGroup A
  renaming (_∙_ to _+_; ε to 0#; _⁻¹ to -_; _-_ to _⊖_; ∙-cong to +-cong)

private
  infixl 7 _-_
  _-_ = _⊖_

open import Algebra.Properties.AbelianGroup A
  renaming (⁻¹-∙-comm to -‿+-comm; ⁻¹-involutive to -‿involutive)

private module ≈-Reasoning = Relation.Binary.Reasoning.Setoid setoid

open ≈-Reasoning

x+y-y≈x : (x y : Carrier) → x + y - y ≈ x
x+y-y≈x x y = begin
  x + y - y   ≈⟨ assoc x y (- y) ⟩
  x + (y - y) ≈⟨ +-cong refl (proj₂ inverse y) ⟩
  x + 0#      ≈⟨ proj₂ identity x ⟩
  x           ∎

x-y+y≈x : (x y : Carrier) → x - y + y ≈ x
x-y+y≈x x y = begin
  x - y + y     ≈⟨ assoc x (- y) y ⟩
  x + (- y + y) ≈⟨ +-cong refl (proj₁ inverse y) ⟩
  x + 0#        ≈⟨ proj₂ identity x ⟩
  x             ∎

x+y+z≈x+z+y : (x y z : Carrier) → x + y + z ≈ x + z + y
x+y+z≈x+z+y x y z = begin
  x + y + z   ≈⟨ assoc x y z ⟩
  x + (y + z) ≈⟨ +-cong refl (comm y z) ⟩
  x + (z + y) ≈⟨ sym (assoc x z y) ⟩
  x + z + y   ∎

x+[y+z]≈y+[x+z] : (x y z : Carrier) → x + (y + z) ≈ y + (x + z)
x+[y+z]≈y+[x+z] x y z = begin
  x + (y + z) ≈⟨ sym (assoc x y z) ⟩
  x + y + z   ≈⟨ +-cong (comm x y) refl ⟩
  y + x + z   ≈⟨ assoc y x z ⟩
  y + (x + z) ∎

-- Note: this function is also in Algebra.Properties.AbelianGroup,
-- but the proof below is more streamlined.
-‿+-comm′ : (x y : Carrier) → - x - y ≈ - (x + y)
-‿+-comm′ x y =
  let eq : x + y + (- y - x) ≈ 0#
      eq = begin
        x + y + (- y - x)   ≈⟨ assoc x y (- y - x) ⟩
        x + (y + (- y - x)) ≈⟨ +-cong refl (sym (assoc y (- y) (- x))) ⟩
        x + (y - y - x)     ≈⟨ +-cong refl (+-cong (proj₂ inverse y) refl) ⟩
        x + (0# - x)        ≈⟨ +-cong refl (proj₁ identity (- x)) ⟩
        x - x               ≈⟨ proj₂ inverse x ⟩
        0#                  ∎
  in  begin
        - x - y                         ≈⟨ comm (- x) (- y) ⟩
        - y - x                         ≈⟨ sym (proj₁ identity (- y - x)) ⟩
        0# + (- y - x)                  ≈⟨ +-cong (sym (proj₁ inverse (x + y))) refl ⟩
        - (x + y) + (x + y) + (- y - x) ≈⟨ assoc (- (x + y)) (x + y) (- y - x) ⟩
        - (x + y) + (x + y + (- y - x)) ≈⟨ +-cong refl eq ⟩
        - (x + y) + 0#                  ≈⟨ proj₂ identity (- (x + y)) ⟩
        - (x + y)                       ∎

-- TODO: better names for the following functions?

-[x-y]≈y-x : (x y : Carrier) → - (x - y) ≈ y - x
-[x-y]≈y-x x y = begin
  - (x - y) ≈⟨ sym (-‿+-comm x (- y)) ⟩
  - x - - y ≈⟨ +-cong refl (-‿involutive y) ⟩
  - x + y   ≈⟨ comm (- x) y ⟩
  y - x     ∎

x-[x+y]≈-y : (x y : Carrier) → x - (x + y) ≈ - y
x-[x+y]≈-y x y = begin
    x - (x + y)   ≈⟨ +-cong refl (sym (-‿+-comm x y)) ⟩
    x + (- x - y) ≈⟨ sym (assoc x (- x) (- y)) ⟩
    x + - x - y   ≈⟨ +-cong (proj₂ inverse x) refl ⟩
    0# - y        ≈⟨ proj₁ identity (- y) ⟩
    - y           ∎

x-[x-y]≈y : (x y : Carrier) → x - (x - y) ≈ y
x-[x-y]≈y x y = begin
    x - (x - y) ≈⟨ x-[x+y]≈-y x (- y) ⟩
    - - y       ≈⟨ -‿involutive y ⟩
    y           ∎

[w+x]+[y+z]≈[w+y]+[x+z] : (w x y z : Carrier) → (w + x) + (y + z) ≈ (w + y) + (x + z)
[w+x]+[y+z]≈[w+y]+[x+z] w x y z = begin
  (w + x) + (y + z)   ≈⟨ assoc w x (y + z) ⟩
  w + (x + (y + z))   ≈⟨ +-cong refl (x+[y+z]≈y+[x+z] x y z) ⟩
  w + (y + (x + z))   ≈⟨ sym (assoc w y (x + z)) ⟩
  w + y + (x + z)     ∎

[w-x]+[y-z]≈[w+y]-[x+z] : (w x y z : Carrier) → (w - x) + (y - z) ≈ (w + y) - (x + z)
[w-x]+[y-z]≈[w+y]-[x+z] w x y z = begin
  (w - x) + (y - z)   ≈⟨ [w+x]+[y+z]≈[w+y]+[x+z] w (- x) y (- z) ⟩
  w + y + (- x - z)   ≈⟨ +-cong refl (-‿+-comm x z) ⟩
  w + y - (x + z)     ∎

y-x≈[z+y]-[z+x] : (x y z : Carrier) → y - x ≈ (z + y) - (z + x)
y-x≈[z+y]-[z+x] x y z = begin
  y - x               ≈⟨ sym (proj₁ identity (y - x)) ⟩
  0# + (y - x)        ≈⟨ +-cong (sym (proj₂ inverse z)) refl ⟩
  (z - z) + (y - x)   ≈⟨ [w-x]+[y-z]≈[w+y]-[x+z] z z y x ⟩
  z + y - (z + x)     ∎

y-x≈[y+z]-[x+z] : (x y z : Carrier) → y - x ≈ (y + z) - (x + z)
y-x≈[y+z]-[x+z] x y z = begin
  y - x                ≈⟨ sym (proj₂ identity (y - x)) ⟩
  y - x + 0#           ≈⟨ +-cong refl (sym (proj₂ inverse z)) ⟩
  y - x + (z - z)      ≈⟨ [w-x]+[y-z]≈[w+y]-[x+z] y x z z ⟩
  y + z - (x + z)      ∎

[x-y]+[y+z]≈x+z : (x y z : Carrier) → x - y + (y + z) ≈ x + z
[x-y]+[y+z]≈x+z x y z = begin
  x - y + (y + z)     ≈⟨ sym (assoc (x - y) y z) ⟩
  (x - y + y) + z     ≈⟨ +-cong (x-y+y≈x x y) refl ⟩
  x + z               ∎

[x+y]+[-y+z]≈x+z : (x y z : Carrier) → x + y + (- y + z) ≈ x + z
[x+y]+[-y+z]≈x+z x y z = begin
  x + y + (- y + z)   ≈⟨ sym (assoc (x + y) (- y) z) ⟩
  (x + y) - y + z     ≈⟨ +-cong (x+y-y≈x x y) refl ⟩
  x + z               ∎
