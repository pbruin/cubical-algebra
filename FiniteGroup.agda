{-# OPTIONS --cubical --safe #-}

-- Finite groups

module FiniteGroup where

open import Algebra using (Group; IsGroup; Op₁; Op₂)

open import Cubical.Core.Primitives
open import Cubical.Data.Nat using (ℕ)

open import Finite using (finite; FiniteSet)

record FiniteGroup ℓ : Type (ℓ-suc ℓ) where
  infix  8 _⁻¹
  infixl 7 _∙_
  field
    G : Type ℓ
    _∙_ : Op₂ G
    ε : G
    _⁻¹ : Op₁ G
    isGroup : IsGroup _≡_ _∙_ ε _⁻¹
    isFinite : finite G

  finiteSet : FiniteSet ℓ
  finiteSet = G , isFinite

  group : Group ℓ ℓ
  group = record { isGroup = isGroup }

  # : ℕ
  # = Finite.# finiteSet
