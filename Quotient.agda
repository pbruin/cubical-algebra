{-# OPTIONS --cubical --safe #-}

-- Functions for set quotients

open import Cubical.Core.Primitives

module Quotient {ℓ} {A : Type ℓ} {ℓ′} {R : A → A → Type ℓ′} where

open import Cubical.Foundations.Function
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Prelude
open import Cubical.HITs.SetQuotients

set-Quotient : isSet (A / R)
set-Quotient = squash/

Quotient-rec : ∀ {ℓ} {S : Type ℓ} → isSet S → (f : A → S)
               → ((a b : A) → R a b → f a ≡ f b)
               → A / R → S
Quotient-rec {S = S} set-S f E = g where
  g : A / R → S
  g [ a ] = f a
  g (eq/ a b r i) = E a b r i
  g (squash/ x y p q i j) = set-S (g x) (g y) (cong g p) (cong g q) i j

Quotient-elim : ∀ {ℓ} {S : A / R → Type ℓ} → ({z : A / R} → isSet (S z))
                → (f : (a : A) → S [ a ])
                → ((a b : A) (r : R a b)
                             → PathP (λ i → S (eq/ a b r i)) (f a) (f b))
                → (z : A / R) → S z
Quotient-elim set-S = elim (λ _ → set-S)

Quotient-elim-prop : ∀ {ℓ} {P : A / R → Type ℓ}
                     → ({z : A / R} → isProp (P z))
                     → (f : (a : A) → P [ a ])
                     → (z : A / R) → P z
Quotient-elim-prop set-S = elimProp (λ _ → set-S)

Quotient-rec² : ∀ {ℓ} {S : Type ℓ} → isSet S → (f : A → A → S)
                → ((a a′ : A) → R a a′ → (b : A) → f a b ≡ f a′ b)
                → ((a b b′ : A) → R b b′ → f a b ≡ f a b′)
                → A / R → A / R → S
Quotient-rec² {S = S} set-S f E₁ E₂ =
  Quotient-rec (isSetΠ (λ _ → set-S)) g
               λ a a′ n∣a-a′ → funExt (Quotient-elim-prop (set-S _ _) (E₁ a a′ n∣a-a′))
  where
    g : A → A / R → S
    g a = Quotient-rec set-S (f a) (E₂ a)

Quotient-elim²-prop : ∀ {ℓ} {P : A / R → A / R → Type ℓ}
                      → ({x y : A / R} → isProp (P x y))
                      → (f : (a b : A) → P [ a ] [ b ])
                      → (x y : A / R) → P x y
Quotient-elim²-prop prop-P f =
  Quotient-elim-prop (isPropΠ λ _ → prop-P)
                     λ a → Quotient-elim-prop prop-P (f a)

Quotient-elim³-prop : ∀ {ℓ} {P : A / R → A / R → A / R → Type ℓ}
                      → ({x y z : A / R} → isProp (P x y z))
                      → (f : (a b c : A) → P [ a ] [ b ] [ c ])
                      → (x y z : A / R) → P x y z
Quotient-elim³-prop prop-P f =
  Quotient-elim-prop (isPropΠ2 λ _ _ → prop-P)
                     λ a → Quotient-elim²-prop prop-P (f a)
