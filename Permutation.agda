{-# OPTIONS --cubical --safe #-}

module Permutation where

open import Cubical.Core.Primitives using (ℓ-max)
open import Cubical.Data.Empty renaming (rec to ⊥-rec)
open import Cubical.Data.Fin renaming (Fin to [_])
open import Cubical.Data.Nat
open import Cubical.Data.Nat.Order using (_<_; ¬-<-zero; ¬m<m)
open import Cubical.Data.Prod
open import Cubical.Foundations.Function using (_∘_; idfun)
open import Cubical.Foundations.Isomorphism using (iso; compIso)
  renaming (Iso to _⟷_)
open import Cubical.Foundations.Prelude
open import Cubical.Relation.Nullary

private
  injective : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′} → (A → B) → Type (ℓ-max ℓ ℓ′)
  injective {A = A} f = {a a′ : A} → f a ≡ f a′ → a ≡ a′

incl : {n : ℕ} → [ n ] → [ suc n ]
incl (i , j , p) = (i , suc j , cong suc p)

injective-incl : {n : ℕ} → injective (incl {n})
injective-incl {n} e = toℕ-injective (cong toℕ e)

Fin0-elim : ∀ {ℓ} {X : Type ℓ} → [ 0 ] → X
Fin0-elim = ⊥-rec ∘ ¬Fin0

Fin-zero : {n : ℕ} → [ suc n ]
Fin-zero {n} = (0 , n , +-comm n 1)

Fin-last : {n : ℕ} → [ suc n ]
Fin-last {n} = (n , 0 , refl)

case-Fin : ∀ {ℓ} {n : ℕ} {A : Type ℓ} (a0 aS : A) → [ n ] → A
case-Fin a0 aS (0 , _)     = a0
case-Fin a0 aS (suc _ , _) = aS

discrete-Fin : {n : ℕ} → Discrete [ n ]
discrete-Fin {n} (i , _) (j , _) with discreteℕ i j
... | yes i≡j = yes (toℕ-injective i≡j)
... | no  i≢j = no λ x≡y → i≢j (cong toℕ x≡y)

{- Bijections -}

bijection : {m n : ℕ} (f : [ m ] → [ n ]) (g : [ n ] → [ m ])
            → ((i : [ m ]) → g (f i) ≡ i) → ((j : [ n ]) → f (g j) ≡ j)
            → [ m ] ⟷ [ n ]
bijection f g invₗ invᵣ = iso f g invᵣ invₗ

_*ᵣ_ : {m n : ℕ} → [ m ] ⟷ [ n ] → [ m ] → [ n ]
_*ᵣ_ = _⟷_.fun

_*ₗ_ : {m n : ℕ} → [ m ] ⟷ [ n ] → [ n ] → [ m ]
_*ₗ_ = _⟷_.inv

inverseₗ : {m n : ℕ} (π : [ m ] ⟷ [ n ]) (i : [ m ]) → π *ₗ (π *ᵣ i) ≡ i
inverseₗ = _⟷_.leftInv

inverseᵣ : {m n : ℕ} (π : [ m ] ⟷ [ n ]) (i : [ n ]) → π *ᵣ (π *ₗ i) ≡ i
inverseᵣ = _⟷_.rightInv

_∘ᵣ_ : {m n o : ℕ} → [ m ] ⟷ [ n ] → [ n ] ⟷ [ o ] → [ m ] ⟷ [ o ]
_∘ᵣ_ = compIso

_∘ₗ_ : {m n o : ℕ} → [ n ] ⟷ [ o ] → [ m ] ⟷ [ n ] → [ m ] ⟷ [ o ]
π′ ∘ₗ π = compIso π π′

_⁻¹ : {m n : ℕ} → [ m ] ⟷ [ n ] → [ n ] ⟷ [ m ]
_⁻¹ (iso f g invᵣ invₗ) = iso g f invₗ invᵣ

adjᵣ : {m n : ℕ} (π : [ m ] ⟷ [ n ]) {i : [ m ]} {j : [ n ]}
       → π *ᵣ i ≡ j → i ≡ π *ₗ j
adjᵣ π {i} E = sym (inverseₗ π i) ∙ cong (π *ₗ_) E

adjₗ : {m n : ℕ} (π : [ m ] ⟷ [ n ]) {i : [ m ]} {j : [ n ]}
       → i ≡ π *ₗ j → π *ᵣ i ≡ j
adjₗ π {j = j} E = cong (π *ᵣ_) E ∙ inverseᵣ π j

injectiveᵣ : {m n : ℕ} (π : [ m ] ⟷ [ n ]) → injective (π *ᵣ_)
injectiveᵣ π {a′ = b} p = adjᵣ π p ∙ inverseₗ π b

injectiveₗ : {m n : ℕ} (π : [ m ] ⟷ [ n ]) → injective (π *ₗ_)
injectiveₗ π {a} p = sym (inverseᵣ π a) ∙ adjₗ π p

{- Permutations -}

Permutation : ℕ → Type₀
Permutation n = [ n ] ⟷ [ n ]

id : (n : ℕ) → Permutation n
id n = bijection (idfun [ n ]) (idfun [ n ]) (λ _ → refl) (λ _ → refl)

-- transposition a <-> b
transposition : {n : ℕ} (a b : [ n ])
                → Σ (Permutation n) λ π → (π *ᵣ a ≡ b) × (π *ᵣ b ≡ a)
transposition {n} a b = iso f f inv inv , fa≡b , fb≡a where
  f : [ n ] → [ n ]
  f x with discrete-Fin x a | discrete-Fin x b
  ... | yes x≡a | _ = b
  ... | no  x≢a | yes x≡b = a
  ... | no  x≢a | no x≢b = x
  fa≡b : f a ≡ b
  fa≡b with discrete-Fin a a | discrete-Fin a b
  ... | yes a≡a | _ = refl
  ... | no  a≢a | _ = ⊥-rec (a≢a refl)
  fb≡a : f b ≡ a
  fb≡a with discrete-Fin b a | discrete-Fin b b
  ... | yes b≡a | _       = b≡a
  ... | no  b≢a | yes b≡b = refl
  ... | no  b≢a | no  b≢b = ⊥-rec (b≢b refl)
  fx≡x : {x : [ n ]} → (¬ x ≡ a) → (¬ x ≡ b) → f x ≡ x
  fx≡x {x} x≢a x≢b with discrete-Fin x a | discrete-Fin x b
  ... | yes x≡a | _       = ⊥-rec (x≢a x≡a)
  ... | no  _   | yes x≡b = ⊥-rec (x≢b x≡b)
  ... | no  _   | no  _   = refl
  inv : (x : [ n ]) → f (f x) ≡ x
  inv x with discrete-Fin x a | discrete-Fin x b
  ... | yes x≡a | _       = fb≡a ∙ sym x≡a
  ... | no  x≢a | yes x≡b = fa≡b ∙ sym x≡b
  ... | no  x≢a | no x≢b  = fx≡x x≢a x≢b

private

  incl≢Fin-last : {n : ℕ} (a : [ n ]) → ¬ incl a ≡ Fin-last
  incl≢Fin-last {n} (i , i<n) eq = ¬m<m n<n where
    n<n : n < n
    n<n = subst (_< n) (cong toℕ eq) i<n

  -- Replace π by a bijection ρ fixing the last element.
  replace : {m n : ℕ} (π : [ suc m ] ⟷ [ suc n ])
           → Σ ([ suc m ] ⟷ [ suc n ])
               λ ρ → (ρ *ᵣ Fin-last ≡ Fin-last) × (ρ *ₗ Fin-last ≡ Fin-last)
  replace {m} {n} π = ρ , eqᵣ , eqₗ where
    a : [ suc n ]
    a = π *ᵣ Fin-last
    x : Σ (Permutation (suc n)) λ σ → (σ *ᵣ a ≡ Fin-last) × (σ *ᵣ Fin-last ≡ a)
    x = transposition a Fin-last
    σ : Permutation (suc n)
    σ = fst x
    ρ : [ suc m ] ⟷ [ suc n ]
    ρ = σ ∘ₗ π
    eqᵣ : ρ *ᵣ Fin-last ≡ Fin-last
    eqᵣ = proj₁ (snd x)
    eqₗ : ρ *ₗ Fin-last ≡ Fin-last
    eqₗ = sym (adjᵣ ρ eqᵣ)

  lemma : {n : ℕ} (a : [ suc n ])
          → ¬ a ≡ Fin-last → Σ [ n ] λ b → a ≡ incl b
  lemma (i , 0 , p) a≢z = ⊥-rec (a≢z (toℕ-injective (cong predℕ p)))
  lemma (i , suc j , p) a≢z = (i , j , cong predℕ p) , toℕ-injective refl

  prop : {m n : ℕ} {f : [ suc m ] → [ suc n ]} → injective f
         → f Fin-last ≡ Fin-last → {i : [ m ]} → ¬ f (incl i) ≡ Fin-last
  prop {f = f} inj p {i} q = incl≢Fin-last i s where
    r : f (incl i) ≡ f Fin-last
    r = q ∙ sym p
    s : incl i ≡ Fin-last
    s = inj r

[1+m]⟷[1+n]→[m]⟷[n] : {m n : ℕ} → [ suc m ] ⟷ [ suc n ]
                         → [ m ] ⟷ [ n ]
[1+m]⟷[1+n]→[m]⟷[n] {m} {n} π = bijection f g invₗ invᵣ where
  ρ : [ suc m ] ⟷ [ suc n ]
  ρ = fst (replace π)
  eqᵣ : ρ *ᵣ Fin-last ≡ Fin-last
  eqᵣ = proj₁ (snd (replace π))
  eqₗ : ρ *ₗ Fin-last ≡ Fin-last
  eqₗ = proj₂ (snd (replace π))
  F : (i : [ m ]) → Σ [ n ] λ j → ρ *ᵣ incl i ≡ incl j
  F i = lemma {n} (ρ *ᵣ incl i) (prop (injectiveᵣ ρ) eqᵣ)
  f : [ m ] → [ n ]
  f i = fst (F i)
  G : (j : [ n ]) → Σ [ m ] λ i → ρ *ₗ incl j ≡ incl i
  G j = lemma {m} (ρ *ₗ incl j) (prop (injectiveₗ ρ) eqₗ)
  g : [ n ] → [ m ]
  g j = fst (G j)
  invₗ : (i : [ m ]) → g (f i) ≡ i
  invₗ i = injective-incl eq″ where
    eq : ρ *ᵣ incl i ≡ incl (f i)
    eq = snd (F i)
    eq′ : ρ *ₗ incl (f i) ≡ incl (g (f i))
    eq′ = snd (G (f i))
    eq″ : incl (g (f i)) ≡ incl i
    eq″ = sym (cong (ρ *ₗ_) eq ∙ eq′) ∙ inverseₗ ρ (incl i)
  invᵣ : (j : [ n ]) → f (g j) ≡ j
  invᵣ j = injective-incl eq″ where
    eq : ρ *ₗ incl j ≡ incl (g j)
    eq = snd (G j)
    eq′ : ρ *ᵣ incl (g j) ≡ incl (f (g j))
    eq′ = snd (F (g j))
    eq″ : incl (f (g j)) ≡ incl j
    eq″ = sym (cong (ρ *ᵣ_) eq ∙ eq′) ∙ inverseᵣ ρ (incl j)

[m]⟷[n]→m≡n : {m n : ℕ} → [ m ] ⟷ [ n ] → m ≡ n
[m]⟷[n]→m≡n {0}  {0}  π = refl
[m]⟷[n]→m≡n {0}  {suc n} π = Fin0-elim (π *ₗ Fin-last)
[m]⟷[n]→m≡n {suc m} {0}  π = Fin0-elim (π *ᵣ Fin-last)
[m]⟷[n]→m≡n {suc m} {suc n} π =
  cong suc ([m]⟷[n]→m≡n ([1+m]⟷[1+n]→[m]⟷[n] π))
