{-# OPTIONS --cubical --safe #-}

module IntegerMod where

open import Algebra using (CommutativeRing)
open import Data.Empty
open import Data.Integer as ℤ hiding (suc; ∣_∣; _<_)
open import Data.Integer.DivMod
open import Data.Integer.Properties
  using (+-*-commutativeRing; +-injective; m-n≡0⇒m≡n; ≤-trans; <-≤-trans;
         m-n≤m; n≮n; *-monoʳ-≤-pos)
open import Data.Nat using (ℕ; suc; _<_; z≤n; s≤s)
open import Relation.Binary.PropositionalEquality using ()
  renaming (_≡_ to _==_; refl to ==-refl; cong to ==-cong;
            sym to ==-sym; subst to ==-subst)
open import Relation.Nullary

open import Cubical.Core.Everything using (ℓ-zero; isEquiv; _≃_)
open import Cubical.Data.Fin renaming (Fin to ℕ<)
import Cubical.Data.Empty
import Cubical.Data.Nat
import Cubical.Data.Nat.Order
open import Cubical.Data.Sigma
open import Cubical.Foundations.Isomorphism using (iso; isoToEquiv)
open import Cubical.Foundations.Prelude hiding (_∎)
open import Cubical.HITs.PropositionalTruncation using (∣_∣)
open import Cubical.HITs.SetQuotients using ([_]; eq/)

open import CommutativeRingQuotient +-*-commutativeRing
open import Finite using (finite; finite⇒set)
open import PrincipalIdeal +-*-commutativeRing
open import Quotient

IntegerMod : (n : ℤ) → CommutativeRing ℓ-zero ℓ-zero
IntegerMod n = CommutativeRingQuotient (PrincipalIdeal n)

open CommutativeRing +-*-commutativeRing hiding (refl; sym; _+_; _*_; _-_; -_)

open import AbelianGroupIdentities +-abelianGroup
open import Algebra.Properties.Ring ring
open import Relation.Binary.Reasoning.Setoid setoid

private
  <-conv : {i n : ℕ} → i < n → i Cubical.Data.Nat.Order.< n
  <-conv {n = suc m} (s≤s z≤n) = m , Cubical.Data.Nat.+-comm m 1
  <-conv {suc i} {suc m} (s≤s (s≤s e)) =
    j , Cubical.Data.Nat.+-suc j (suc i) ∙ cong suc j+1+i≡m where
    x = <-conv (s≤s e)
    j = fst x
    j+1+i≡m = snd x

  <-conv′ : {i n : ℕ} → i Cubical.Data.Nat.Order.< n → i < n
  <-conv′ {n = 0} e = Cubical.Data.Empty.rec (Cubical.Data.Nat.Order.¬-<-zero e)
  <-conv′ {0} {suc n} _ = s≤s z≤n
  <-conv′ {suc i} {suc n} (j , j+2+i≡1+n) = s≤s (<-conv′ (j , e)) where
    e = Cubical.Data.Nat.injSuc (sym (Cubical.Data.Nat.+-suc j (suc i)) ∙ j+2+i≡1+n)

module equiv (n-1 : ℕ) where
  n : ℕ
  n = suc n-1

  ℕ<-cong : {a b : ℕ< n} → toℕ a == toℕ b → a ≡ b
  ℕ<-cong ==-refl = toℕ-injective refl

  mod< : ℤ → ℕ< n
  mod< a = (a modℕ n , <-conv (n%ℕd<d a n))

  unique₀ : (i j : ℕ) → i < n → (r : ℕ) → ¬ (+ i - + j == + suc r * + n)
  unique₀ i j i<n r i-j==qn = n≮n i<i where
    q qn : ℤ
    q = + suc r
    qn = q * + n
    1≤q : + 1 ≤ q
    1≤q = +≤+ (s≤s z≤n)
    n≤qn : + n ≤ qn
    n≤qn = ==-subst (_≤ qn) (*-identityˡ (+ n)) (*-monoʳ-≤-pos n-1 1≤q)
    qn≤i : qn ≤ + i
    qn≤i = ==-subst (_≤ + i) i-j==qn (m-n≤m (+ i) j)
    i<i : + i ℤ.< + i
    i<i = <-≤-trans {+ i} (+<+ i<n) (≤-trans n≤qn qn≤i)

  unique : {i j : ℕ} → i < n → j < n → (q : ℤ) → + i - + j == q * + n
           → i == j
  unique {i} {j} i<n j<n (+ 0) i-j==qn =
    +-injective (m-n≡0⇒m≡n (+ i) (+ j)
                           (trans i-j==qn (zeroˡ (+ n))))
  unique {i} {j} i<n j<n (+ suc r) i-j==qn = ⊥-elim (unique₀ i j i<n r i-j==qn)
  unique {i} {j} i<n j<n -[1+ r ] i-j==qn = ⊥-elim (unique₀ j i j<n r j-i==-qn) where
    j-i==-qn : + j - + i == + suc r * + n
    j-i==-qn = trans (==-sym (-[x-y]≈y-x (+ i) (+ j))) (-‿cong i-j==qn)

  lemma : (a b : ℤ) → Σ ℤ (λ q → a - b == q * + n)
          → mod< a ≡ mod< b
  lemma a b (q , a-b==qn) = ℕ<-cong i==j where
    i j : ℕ
    i = a modℕ n
    j = b modℕ n
    i<n : i < n
    i<n = n%ℕd<d a n
    j<n : j < n
    j<n = n%ℕd<d b n
    u v : ℤ
    u = a divℕ n
    v = b divℕ n
    δ ζ qn un vn : ℤ
    δ = + i - + j
    ζ = (u - v) * + n
    qn = q * + n
    un = u * + n
    vn = v * + n
    eq1 : a == + i + un
    eq1 = a≡a%ℕn+[a/ℕn]*n a n
    eq2 : b == + j + vn
    eq2 = a≡a%ℕn+[a/ℕn]*n b n
    eq3 : a - b == δ + (u - v) * + n
    eq3 = begin
      a - b                  ≈⟨ +-cong eq1 (-‿cong eq2) ⟩
      + i + un - (+ j + vn)  ≈⟨ ==-sym ([w-x]+[y-z]≈[w+y]-[x+z] (+ i) (+ j) un vn) ⟩
      δ + (un - vn)          ≈⟨ ==-cong (λ x → δ + (un + x)) (==-sym (-‿*-distribˡ v (+ n))) ⟩
      δ + (un + (- v) * + n) ≈⟨ ==-cong (λ x → δ + x) (==-sym (distribʳ (+ n) u (- v))) ⟩
      δ + (u - v) * + n      ∎
    eq4 : + i - + j == (q - (u - v)) * + n
    eq4 = begin
      δ                      ≈⟨ ==-sym (x+y-y≈x δ ζ) ⟩
      δ + ζ - ζ              ≈⟨ ==-cong (_- ζ) (==-sym eq3) ⟩
      a - b - ζ              ≈⟨ ==-cong (_- ζ) a-b==qn ⟩
      qn - ζ                 ≈⟨ ==-cong (λ x → qn + x) (==-sym (-‿*-distribˡ (u - v) (+ n))) ⟩
      qn + (- (u - v)) * + n ≈⟨ ==-sym (distribʳ (+ n) q (- (u - v))) ⟩
      (q - (u - v)) * + n    ∎
    i==j : i == j
    i==j = unique i<n j<n (q - (u - v)) eq4

  open CommutativeRing (IntegerMod (+ n)) using () renaming (Carrier to ℤ/nℤ)

  f : ℤ/nℤ → ℕ< n
  f = Quotient-rec isSetFin mod< lemma

  g : ℕ< n → ℤ/nℤ
  g r = [ + toℕ r ]

  claim-gf-pre : (a : ℤ) → g (f [ a ]) ≡ [ a ]
  claim-gf-pre a = eq/ (+ toℕ (mod< a)) a (- q , eq) where
    q qn : ℤ
    q = a divℕ n
    qn = q * + n
    r : ℕ
    r = a modℕ n
    eq : + r - a == - q * + n
    eq = begin
      + r - a            ≈⟨ ==-cong (λ x → + r - x) (a≡a%ℕn+[a/ℕn]*n a n) ⟩
      + r - (+ r + qn)   ≈⟨ x-[x+y]≈-y (+ r) qn ⟩
      - qn               ≈⟨ ==-sym (-‿*-distribˡ q (+ n)) ⟩
      - q * + n ∎

  claim-gf : (a : ℤ/nℤ) → g (f a) ≡ a
  claim-gf = Quotient-elim-prop (set-Quotient _ _) claim-gf-pre

  claim-fg : (x : ℕ< n) → f (g x) ≡ x
  claim-fg (i , y) = ℕ<-cong (==-sym i==r) where
    r : ℕ
    r = + i modℕ n
    r<n : r < n
    r<n = n%ℕd<d (+ i) n
    q qn : ℤ
    q = + i divℕ n
    qn = q * + n
    i-r==qn : + i - + r == qn
    i-r==qn = begin
      + i - + r        ≈⟨ ==-cong (λ x → x - + r) (a≡a%ℕn+[a/ℕn]*n (+ i) n) ⟩
      (+ r + qn) - + r ≈⟨ ==-cong (λ x → x - + r) (+-comm (+ r) qn) ⟩
      (qn + + r) - + r ≈⟨ x+y-y≈x qn (+ r) ⟩
      qn               ∎
    i==r : i == r
    i==r = unique (<-conv′ y) r<n q i-r==qn

  ℤ/nℤ≃ℕ<n : ℤ/nℤ ≃ ℕ< n
  ℤ/nℤ≃ℕ<n = isoToEquiv (iso f g claim-fg claim-gf)

  ℕ<n≃ℤ/nℤ : ℕ< n ≃ ℤ/nℤ
  ℕ<n≃ℤ/nℤ = isoToEquiv (iso g f claim-gf claim-fg)

  finite-ℤ/nℤ : finite ℤ/nℤ
  finite-ℤ/nℤ = ∣ n , ℤ/nℤ≃ℕ<n ∣

  set-ℤ/nℤ : isSet ℤ/nℤ
  set-ℤ/nℤ = finite⇒set finite-ℤ/nℤ

open equiv public using (ℤ/nℤ≃ℕ<n; ℕ<n≃ℤ/nℤ; finite-ℤ/nℤ; set-ℤ/nℤ)
