{-# OPTIONS --without-K --safe #-}

open import Algebra using (CommutativeRing)

module PrincipalIdeal {c} {ℓ} (A : CommutativeRing c ℓ) where

open import Agda.Primitive using (_⊔_)
open import Data.Product using (Σ; _,_)

import Relation.Binary.Reasoning.Setoid
private module ≈-Reasoning = Relation.Binary.Reasoning.Setoid (CommutativeRing.setoid A)
open ≈-Reasoning

open CommutativeRing A renaming (Carrier to C)
open import Algebra.Properties.Ring (CommutativeRing.ring A)
open import Ideal (CommutativeRing.ring A) using (Ideal)

PrincipalIdeal : (r : C) → Ideal (c ⊔ ℓ)
PrincipalIdeal r = record
  {
    subgroup = record
      {
        contains = contains ;
        contains-≈ = contains-≈ ;
        contains-ε = (0# , sym (zeroˡ r)) ;
        mul = add ;
        inv = neg
      } ;
    lmul = lmul ;
    rmul = rmul
  } where
    contains : C → Set (c ⊔ ℓ)
    contains a = Σ C λ q → a ≈ q * r

    contains-≈ : {a b : C} → a ≈ b → contains a → contains b
    contains-≈ a≈b (q , a≈qr) = (q , trans (sym a≈b) a≈qr)

    add : {a b : C} → contains a → contains b → contains (a + b)
    add {a} {b} (q , a≈qr) (q′ , b≈q′r) = q + q′ , eq where
      eq : a + b ≈ (q + q′) * r
      eq = begin
        a + b          ≈⟨ +-cong a≈qr b≈q′r ⟩
        q * r + q′ * r ≈⟨ sym (distribʳ r q q′) ⟩
        (q + q′) * r   ∎

    neg : {a : C} → contains a → contains (- a)
    neg {a} (q , a≈qr) = - q , eq where
      eq : - a ≈ - q * r
      eq = begin
        - a       ≈⟨ -‿cong a≈qr ⟩
        - (q * r) ≈⟨ sym (-‿*-distribˡ q r) ⟩
        - q * r   ∎

    lmul : (a : C) {b : C} → contains b → contains (a * b)
    lmul a {b} (q , b≈qr) = a * q , eq where
      eq : a * b ≈ a * q * r
      eq = begin
        a * b       ≈⟨ *-cong (refl {a}) b≈qr ⟩
        a * (q * r) ≈⟨ sym (*-assoc a q r) ⟩
        a * q * r   ∎

    rmul : (a : C) {b : C} → contains b → contains (b * a)
    rmul a {b} z = contains-≈ (*-comm a b) (lmul a z)
