{-# OPTIONS --cubical --safe #-}

module Equiv where

open import Cubical.Core.Everything using (ℓ-max; isEquiv; equiv-proof)
open import Cubical.Data.Sigma
open import Cubical.Functions.Embedding
  using (isEmbedding; hasPropFibers; hasPropFibers→isEmbedding)
open import Cubical.Foundations.Equiv using (fiber; invEquiv)
open import Cubical.Foundations.Prelude
open import Cubical.HITs.PropositionalTruncation using (∥_∥)
open import Cubical.Relation.Nullary using (Dec; Discrete; yes; no)

isEquiv→hasPropFibers : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′} {f : A → B}
                        → isEquiv f → hasPropFibers f
isEquiv→hasPropFibers E b = isContr→isProp (equiv-proof E b)

isEquiv→isEmbedding : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′} {f : A → B}
                      → isEquiv f → isEmbedding f
isEquiv→isEmbedding E = hasPropFibers→isEmbedding (isEquiv→hasPropFibers E)

isEmbedding-preserves-isSet-inv : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′} {f : A → B}
                                  → isEmbedding f → isSet B → isSet A
isEmbedding-preserves-isSet-inv {f = f} emb S a a′ p q = p≡q where
  [f] : a ≡ a′ → f a ≡ f a′
  [f] = cong f
  [f]p≡[f]q : [f] p ≡ [f] q
  [f]p≡[f]q = S (f a) (f a′) ([f] p) ([f] q)
  eq : isEquiv [f]
  eq = emb a a′
  s : (h : f a ≡ f a′) → fiber [f] h
  s h = fst (equiv-proof (emb a a′) h)
  Fp Fp′ : fiber [f] ([f] p)
  Fp = (p , refl)
  Fp′ = s ([f] p)
  Cp : Fp′ ≡ Fp
  Cp = isContr→isProp (equiv-proof eq ([f] p)) Fp′ Fp
  Fq Fq′ : fiber [f] ([f] q)
  Fq = (q , refl)
  Fq′ = s ([f] q)
  Cq : Fq′ ≡ Fq
  Cq = isContr→isProp (equiv-proof eq ([f] q)) Fq′ Fq
  s[f]p s[f]q : a ≡ a′
  s[f]p = fst Fp′
  s[f]p≡p : s[f]p ≡ p
  s[f]p≡p = λ i → fst (Cp i)
  s[f]q = fst Fq′
  s[f]q≡q : s[f]q ≡ q
  s[f]q≡q = λ i → fst (Cq i)
  s[f]p≡s[f]q : s[f]p ≡ s[f]q
  s[f]p≡s[f]q = cong (λ g → fst (s g)) [f]p≡[f]q
  p≡q : p ≡ q
  p≡q = transport (λ i → s[f]p≡p i ≡ s[f]q≡q i) s[f]p≡s[f]q

isEquiv-preserves-isSet-inv : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′}
                              {f : A → B} → isEquiv f
                              → isSet B → isSet A
isEquiv-preserves-isSet-inv E =
  isEmbedding-preserves-isSet-inv (isEquiv→isEmbedding E)

logical-equiv-preserves-Dec : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′}
                              (f : A → B) (g : B → A) →
                              Dec A → Dec B
logical-equiv-preserves-Dec f g =
  λ { (yes a) → yes (f a) ; (no ¬a) → no (λ b → ¬a (g b)) }

isEquiv-preserves-Dec : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′}
                        {f : A → B} → isEquiv f
                        → Dec A → Dec B
isEquiv-preserves-Dec {f = f} E =
  logical-equiv-preserves-Dec f λ b → fst (fst (equiv-proof E b))

isEquiv-preserves-Dec-inv : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′}
                            {f : A → B} → isEquiv f
                            → Dec B → Dec A
isEquiv-preserves-Dec-inv {f = f} E =
  logical-equiv-preserves-Dec (λ b → fst (fst (equiv-proof E b))) f

isEmbedding-preserves-Discrete-inv : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′}
                                     {f : A → B} → isEmbedding f
                                     → Discrete B → Discrete A
isEmbedding-preserves-Discrete-inv {f = f} emb D a a′ = dA where
  dB : Dec (f a ≡ f a′)
  dB = D (f a) (f a′)
  dA : Dec (a ≡ a′)
  dA = isEquiv-preserves-Dec-inv (emb a a′) dB

isEquiv-preserves-Discrete-inv : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′}
                                 {f : A → B} → isEquiv f
                                 → Discrete B → Discrete A
isEquiv-preserves-Discrete-inv E =
  isEmbedding-preserves-Discrete-inv (isEquiv→isEmbedding E)

isEquiv-preserves-discrete : ∀ {ℓ ℓ′} {A : Type ℓ} {B : Type ℓ′}
                             {f : A → B} → isEquiv f
                             → Discrete A → Discrete B
isEquiv-preserves-discrete {f = f} E =
  isEquiv-preserves-Discrete-inv (snd (invEquiv (f , E)))
