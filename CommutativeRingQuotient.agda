{-# OPTIONS --cubical --safe #-}

-- Quotient of a commutative ring by an ideal

open import Algebra using (CommutativeRing; Ring)
open import Ideal using (Ideal)

module CommutativeRingQuotient {c} {ℓ} (A : CommutativeRing c ℓ)
  {ι} (𝕒 : Ideal (CommutativeRing.ring A) ι) where

open import Cubical.Core.Primitives using (_≡_; ℓ-max)

import RingQuotient

CommutativeRingQuotient : CommutativeRing (ℓ-max c ι) (ℓ-max c ι)
CommutativeRingQuotient = record { isCommutativeRing = isCommutativeRing } where

  open Ring (RingQuotient.RingQuotient 𝕒) renaming (Carrier to A/𝕒)
  open import Algebra using (IsCommutativeRing)
  open import Algebra.Definitions (_≡_ {A = A/𝕒}) using (Commutative)

  *-comm : Commutative (_*_)
  *-comm = Quotient-elim²-prop (set-Quotient _ _)
                               λ a b → []-congₗ (CommutativeRing.*-comm A a b)
    where
      open import LeftRightQuotient (Ideal.subgroup 𝕒)
      open import Quotient

  isCommutativeRing : IsCommutativeRing _≡_ _+_ _*_ -_ 0# 1#
  isCommutativeRing = record { isRing = isRing ; *-comm = *-comm }
