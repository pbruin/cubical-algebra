cubical-algebra
===============

A formalisation of some algebraic constructions in cubical type theory
using the Agda programming language.


Features
--------

- Finite sets and finite groups

- Quotient groups and quotient rings

- Integers modulo <i>n</i> as a commutative ring and as a finite
  cyclic group


Author
------

Peter Bruin, <P.J.Bruin@math.leidenuniv.nl>


Prerequisites
-------------

- A recent version of Agda, including the Agda standard library,
  <http://wiki.portal.chalmers.se/agda/pmwiki.php>

- The `cubical` Agda library, <https://github.com/agda/cubical/>
