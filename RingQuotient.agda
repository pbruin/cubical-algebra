{-# OPTIONS --cubical --safe #-}

-- Quotient of a ring by a two-sided ideal

open import Algebra using (AbelianGroup; Ring)
import Ideal

module RingQuotient {c} {ℓ} {A : Ring c ℓ} {ι} (𝕒 : Ideal.Ideal A ι) where

open import Cubical.Core.Primitives using (_≡_; ℓ-max)
open import Cubical.Data.Sigma using (_,_; fst; snd)
open import Cubical.HITs.SetQuotients

open import Path using (congruence₂; ≡-isEquivalence)

open Ring A renaming (Carrier to C; refl to ≈-refl)

import Relation.Binary.Reasoning.Setoid
private module ≈-Reasoning = Relation.Binary.Reasoning.Setoid setoid
open ≈-Reasoning

open Ideal A using (_∈_)
open Ideal.Ideal 𝕒
open import LeftRightQuotient subgroup renaming (LeftQuotient to A/𝕒)
open import AbelianGroupQuotient +-abelianGroup subgroup
open import Quotient

open AbelianGroup AbelianGroupQuotient hiding (_≈_; _-_)
  renaming (_∙_ to _⊕_; _⁻¹ to ⊖_)

open import Algebra using (IsRing)
open import Algebra.Definitions (_≡_ {A = A/𝕒})
open import Algebra.Properties.Ring A

RingQuotient : Ring (ℓ-max c ι) (ℓ-max c ι)
RingQuotient = record { isRing = ⊕-⊗-isRing } where

  lemma-⊗-1 : (a b : C) → a - b ∈ 𝕒 → (c : C) → [ a * c ] ≡ [ b * c ]
  lemma-⊗-1 a b a-b∈𝕒 c = eq/ (a * c) (b * c) ac-bc∈𝕒 where
    eq : (a - b) * c ≈ a * c - b * c
    eq = begin
      (a - b) * c       ≈⟨ snd distrib c a (- b) ⟩
      a * c + (- b) * c ≈⟨ +-cong ≈-refl (-‿*-distribˡ b c) ⟩
      a * c - b * c ∎
    ac-bc∈𝕒 : a * c - b * c ∈ 𝕒
    ac-bc∈𝕒 = contains-≈ eq (rmul c a-b∈𝕒)

  lemma-⊗-2 : (m a b : C) → a - b ∈ 𝕒 → [ m * a ] ≡ [ m * b ]
  lemma-⊗-2 m a b a-b∈𝕒 = eq/ (m * a) (m * b) ma-mb∈𝕒 where
    eq : m * (a - b) ≈ m * a - m * b
    eq = begin
      m * (a - b)     ≈⟨ fst distrib m a (- b) ⟩
      m * a + m * - b ≈⟨ +-cong ≈-refl (-‿*-distribʳ m b) ⟩
      m * a - m * b   ∎
    ma-mb∈𝕒 : m * a - m * b ∈ 𝕒
    ma-mb∈𝕒 = contains-≈ eq (lmul m a-b∈𝕒)

  _⊗_ : A/𝕒 → A/𝕒 → A/𝕒
  _⊗_ = Quotient-rec² set-Quotient (λ a b → [ a * b ]) lemma-⊗-1 lemma-⊗-2

  ⊗-assoc : Associative _⊗_
  ⊗-assoc = Quotient-elim³-prop (set-Quotient _ _)
                                λ a b c → []-congₗ (*-assoc a b c)

  ⊗-idₗ : LeftIdentity [ 1# ] _⊗_
  ⊗-idₗ = Quotient-elim-prop (set-Quotient _ _)
                             λ a → []-congₗ (*-identityˡ a)

  ⊗-idᵣ : RightIdentity [ 1# ] _⊗_
  ⊗-idᵣ = Quotient-elim-prop (set-Quotient _ _)
                             λ a → []-congₗ (*-identityʳ a)

  ⊗-⊕-distribₗ : _⊗_ DistributesOverˡ _⊕_
  ⊗-⊕-distribₗ = Quotient-elim³-prop (set-Quotient _ _)
                                     λ a b c → []-congₗ (distribˡ a b c)

  ⊗-⊕-distribᵣ : _⊗_ DistributesOverʳ _⊕_
  ⊗-⊕-distribᵣ = Quotient-elim³-prop (set-Quotient _ _)
                                     λ a b c → []-congₗ (distribʳ a b c)

  zeroₗ : LeftZero [ 0# ] _⊗_
  zeroₗ = Quotient-elim-prop (set-Quotient _ _)
                             λ a → []-congₗ (zeroˡ a)

  zeroᵣ : RightZero [ 0# ] _⊗_
  zeroᵣ = Quotient-elim-prop (set-Quotient _ _)
                             λ a → []-congₗ (zeroʳ a)

  ⊕-⊗-isRing : IsRing _≡_ _⊕_ _⊗_ ⊖_ [ 0# ] [ 1# ]
  ⊕-⊗-isRing = record
    {
      +-isAbelianGroup = isAbelianGroup ;
      *-isMonoid = record
        {
          isSemigroup = record
            {
              isMagma = record
                {
                  isEquivalence = ≡-isEquivalence ;
                  ∙-cong = congruence₂ _⊗_
                } ;
              assoc = ⊗-assoc
            } ;
          identity = ⊗-idₗ , ⊗-idᵣ
        } ;
      distrib = ⊗-⊕-distribₗ , ⊗-⊕-distribᵣ ;
      zero = zeroₗ , zeroᵣ
    }
