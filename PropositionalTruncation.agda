{-# OPTIONS --cubical --safe #-}

module PropositionalTruncation where

open import Cubical.Core.Primitives using (Type)
open import Cubical.Foundations.HLevels using (isPropΠ)
open import Cubical.Foundations.Prelude using (isProp)
open import Cubical.HITs.PropositionalTruncation

-- generalisation of Cubical.HITs.PropositionalTruncation.rec2
recPropTrunc² : ∀ {ℓ ℓ′ ℓ″} {A : Type ℓ} {B : Type ℓ′} {C : Type ℓ″}
                → isProp C → (A → B → C) → ∥ A ∥ → ∥ B ∥ → C
recPropTrunc² P f =
  rec (isPropΠ λ _ → P) (λ a → rec P (f a))
