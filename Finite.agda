{-# OPTIONS --cubical --safe #-}

-- Finite sets

module Finite where

open import Cubical.Core.Everything using (_≡_; ℓ-suc; _≃_)
open import Cubical.Data.Fin
open import Cubical.Data.Nat using (ℕ; isSetℕ)
open import Cubical.Data.Sigma
open import Cubical.Foundations.Equiv using (invEquiv; equivToIso)
open import Cubical.Foundations.Function using (_∘_; idfun)
open import Cubical.Foundations.HLevels using (isPropIsSet; isPropΠ2)
open import Cubical.Foundations.Isomorphism using (iso; compIso)
  renaming (Iso to _⟷_)
open import Cubical.Foundations.Prelude
open import Cubical.HITs.PropositionalTruncation
open import Cubical.Relation.Nullary using (Dec; Discrete; isPropDec)

open import Equiv
open import Permutation
open import PropositionalTruncation

finite : ∀ {ℓ} (A : Type ℓ) → Type ℓ
finite A = ∥ Σ ℕ (λ n → A ≃ Fin n) ∥

finite′ : ∀ {ℓ} (A : Type ℓ) → Type ℓ
finite′ A = Σ ℕ λ n → ∥ A ≃ Fin n ∥

finite′⇒finite : ∀ {ℓ} {A : Type ℓ} → finite′ A → finite A
finite′⇒finite {A = A} (n , x) =
  rec propTruncIsProp (λ x → ∣ n , x ∣) x

proposition-finite′ : ∀ {ℓ} (A : Type ℓ) → isProp (finite′ A)
proposition-finite′ {ℓ} A (m , x) (n , y) = J P P-refl m≡n y where
  claim : A ≃ Fin m → A ≃ Fin n → Fin m ⟷ Fin n
  claim e f = compIso p q
    where p = equivToIso (invEquiv e)
          q = equivToIso f
  claim′ : A ≃ Fin m → A ≃ Fin n → m ≡ n
  claim′ e f = [m]⟷[n]→m≡n (claim e f)
  m≡n : m ≡ n
  m≡n = recPropTrunc² (isSetℕ m n) claim′ x y
  P : (s : ℕ) → m ≡ s → Type ℓ
  P s m≡s = (z : ∥ A ≃ Fin s ∥) → (m , x) ≡ (s , z)
  P-refl : P m refl
  P-refl z = λ i → m , p i where
    p : x ≡ z
    p = propTruncIsProp x z

finite⇒finite′ : ∀ {ℓ} (A : Type ℓ) → finite A → finite′ A
finite⇒finite′ A q = rec (proposition-finite′ A) w q where
  w : Σ ℕ (λ n → A ≃ Fin n) → finite′ A
  w (n , x) = n , ∣ x ∣

FiniteSet : ∀ ℓ → Type (ℓ-suc ℓ)
FiniteSet ℓ = Σ (Type ℓ) finite

# : ∀ {ℓ} → FiniteSet ℓ → ℕ
# (A , x) = fst (finite⇒finite′ A x)

finite⇒set : ∀ {ℓ} {A : Type ℓ} → finite A → isSet A
finite⇒set {A = A} = rec isPropIsSet claim where
  claim : Σ ℕ (λ n → A ≃ Fin n) → isSet A
  claim (n , (f , E)) = isEquiv-preserves-isSet-inv E isSetFin

set-FiniteSet : ∀ {ℓ} (A : FiniteSet ℓ) → isSet (fst A)
set-FiniteSet (A , x) = finite⇒set x

finite⇒discrete : ∀ {ℓ} {A : Type ℓ} → finite A → Discrete A
finite⇒discrete {A = A} x = rec prop-discrete claim x where
  prop-dec : {a a′ : A} → isProp (Dec (a ≡ a′))
  prop-dec = isPropDec (finite⇒set x _ _)
  prop-discrete : isProp (Discrete A)
  prop-discrete = isPropΠ2 λ y z → prop-dec
  claim : Σ ℕ (λ n → A ≃ Fin n) → Discrete A
  claim (n , (f , E)) = isEquiv-preserves-Discrete-inv E discrete-Fin

discrete-FiniteSet : ∀ {ℓ} (A : FiniteSet ℓ) → Discrete (fst A)
discrete-FiniteSet (A , x) = finite⇒discrete x
