{-# OPTIONS --cubical --safe #-}

-- Finite cyclic groups

module CyclicGroup where

open import Algebra using (CommutativeRing; Group)
open import Data.Integer using (+_)

open import Cubical.Core.Primitives using (_≡_; ℓ-zero)
open import Cubical.Data.Empty renaming (rec to ⊥-rec)
open import Cubical.Data.Nat using (ℕ; suc)
open import Cubical.Data.Nat.Order using (_<_; ¬-<-zero)
open import Cubical.Foundations.Prelude using (refl)

open import FiniteGroup using (FiniteGroup)
open import IntegerMod using (IntegerMod; finite-ℤ/nℤ)

CyclicGroup : {n : ℕ} → 0 < n → FiniteGroup ℓ-zero
CyclicGroup {0} 0<n = ⊥-rec (¬-<-zero 0<n)
CyclicGroup {suc n-1} _ = record
  {
    isGroup = Group.isGroup (CommutativeRing.+-group (IntegerMod (+ suc n-1))) ;
    isFinite = finite-ℤ/nℤ n-1
  }

#Cₙ≡n : {n : ℕ} (0<n : 0 < n) → FiniteGroup.# (CyclicGroup 0<n) ≡ n
#Cₙ≡n {0} 0<n = ⊥-rec (¬-<-zero 0<n)
#Cₙ≡n {suc n-1} _ = refl
