{-# OPTIONS --without-K --safe #-}

-- Subgroups

open import Algebra using (Group)

module Subgroup {c} {ℓ} (G : Group c ℓ) where

open import Agda.Primitive using (lsuc; _⊔_)

open Group G renaming (Carrier to C)

record Subgroup ι : Set (c ⊔ ℓ ⊔ lsuc ι) where
  field
    contains : C → Set ι
    contains-≈ : {a b : C} → a ≈ b → contains a → contains b
    contains-ε : contains ε
    mul : {a b : C} → contains a → contains b → contains (a ∙ b)
    inv : {a : C} → contains a → contains (a ⁻¹)

  group : Group (c ⊔ ι) ℓ
  group = record
    {
      Carrier = Σ C contains ;
      _≈_ = λ { (a , _) (b , _) → a ≈ b } ;
      _∙_ = λ { (a , ca) (b , cb) → a ∙ b , mul ca cb } ;
      ε = ε , contains-ε ;
      _⁻¹ = λ { (a , ca) → a ⁻¹ , inv ca } ;
      isGroup = record
        {
          isMonoid = record
            {
              isSemigroup = record
                {
                  isMagma = record
                    {
                      isEquivalence = record
                        {
                          refl = refl ;
                          sym = sym ;
                          trans = trans
                        } ;
                      ∙-cong = ∙-cong
                    } ;
                  assoc = λ { (a , _) (b , _) (c , _) → assoc a b c }
                } ;
              identity = (λ { (a , _) → fst identity a }) ,
                         (λ { (a , _) → snd identity a })
            } ;
          inverse = (λ { (a , _) → fst inverse a }) ,
                    (λ { (a , _) → snd inverse a }) ;
          ⁻¹-cong = ⁻¹-cong
        }
    }
    where open import Agda.Builtin.Sigma

infix 2 _∈_

_∈_ : ∀ {ℓ} → C → Subgroup ℓ → Set ℓ
a ∈ 𝕒 = Subgroup.contains 𝕒 a
