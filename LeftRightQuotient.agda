{-# OPTIONS --cubical --safe #-}

-- Left and right quotients of a group by a subgroup (as sets)

open import Algebra using (Group)
import Subgroup

module LeftRightQuotient {c} {ℓ} {G : Group c ℓ} {ι} (H : Subgroup.Subgroup G ι) where

open import Cubical.Core.Primitives using (Type; _≡_; ℓ-max)
open import Cubical.HITs.SetQuotients

open Group G
open Subgroup G

LeftQuotient : Type (ℓ-max c ι)
LeftQuotient = Carrier / (λ a b → a ∙ b ⁻¹ ∈ H)

RightQuotient : Type (ℓ-max c ι)
RightQuotient = Carrier / (λ a b → a ⁻¹ ∙ b ∈ H)

import Relation.Binary.Reasoning.Setoid
private module ≈-Reasoning = Relation.Binary.Reasoning.Setoid setoid
open ≈-Reasoning

[]-congₗ : {a b : Carrier} → a ≈ b → _≡_ {A = LeftQuotient} [ a ] [ b ]
[]-congₗ {a} {b} a≈b =
  eq/ a b (Subgroup.contains-≈ H eq (Subgroup.contains-ε H)) where
    eq : ε ≈ a ∙ b ⁻¹
    eq = begin
      ε        ≈⟨ sym (inverseʳ a) ⟩
      a ∙ a ⁻¹ ≈⟨ ∙-cong refl (⁻¹-cong a≈b) ⟩
      a ∙ b ⁻¹ ∎

[]-congᵣ : {a b : Carrier} → a ≈ b → _≡_ {A = RightQuotient} [ a ] [ b ]
[]-congᵣ {a} {b} a≈b =
  eq/ a b (Subgroup.contains-≈ H eq (Subgroup.contains-ε H)) where
    eq : ε ≈ a ⁻¹ ∙ b
    eq = begin
      ε        ≈⟨ sym (inverseˡ a) ⟩
      a ⁻¹ ∙ a ≈⟨ ∙-cong refl a≈b ⟩
      a ⁻¹ ∙ b ∎
