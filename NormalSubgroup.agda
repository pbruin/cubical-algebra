{-# OPTIONS --without-K --safe #-}

-- Normal subgroups

open import Algebra using (Group)

module NormalSubgroup {c} {ℓ} (G : Group c ℓ) where

open import Agda.Primitive using (lsuc; _⊔_)

open Group G renaming (Carrier to C)
open import Subgroup G hiding (_∈_)

import Relation.Binary.Reasoning.Setoid
private module ≈-Reasoning = Relation.Binary.Reasoning.Setoid setoid
open ≈-Reasoning

record NormalSubgroup ι : Set (c ⊔ ℓ ⊔ lsuc ι) where
  field
    subgroup : Subgroup ι

  open Subgroup subgroup public

  field
    normal : (g h : C) → contains (g ∙ h) → contains (h ∙ g)

  conj : (g : C) {n : C} → contains n → contains (g ∙ n ∙ g ⁻¹)
  conj g {n} n∈N = normal (g ⁻¹) (g ∙ n) (contains-≈ eq n∈N) where
    eq : n ≈ g ⁻¹ ∙ (g ∙ n)
    eq = begin
      n              ≈⟨ sym (identityˡ n) ⟩
      ε ∙ n          ≈⟨ ∙-cong (sym (inverseˡ g)) refl ⟩
      g ⁻¹ ∙ g ∙ n   ≈⟨ assoc (g ⁻¹) g n ⟩
      g ⁻¹ ∙ (g ∙ n) ∎

infix 2 _∈_

_∈_ : ∀ {ℓ} → C → NormalSubgroup ℓ → Set ℓ
a ∈ N = NormalSubgroup.contains N a
